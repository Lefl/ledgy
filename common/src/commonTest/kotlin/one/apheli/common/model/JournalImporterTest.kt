package one.apheli.common.model

import java.math.BigDecimal
import java.time.LocalDate
import kotlin.test.Test
import kotlin.test.assertEquals

internal class JournalImporterTest {
    val transactionCount = 1
    @Test
    fun testJournalImport() {
        val stream = this.javaClass.getResourceAsStream("/testjournal.journal")
        val journal = importJournal(stream!!, "testjournal.journal")
        assertEquals(transactionCount, journal.transactions.size)
        testSimpleTransaction(journal.transactions[0])
    }

    private fun testSimpleTransaction(tx: Transaction) {
        assertEquals(LocalDate.parse("2000-02-28"), tx.date)
        assertEquals(Status.PENDING, tx.status)
        assertEquals("#0431", tx.code)
        assertEquals("Simple Transaction", tx.description)
        // TODO: Implement comments assertEquals("This is a comment", tx.comment)

        /* Test postings */
        val postings = tx.postings
        assertEquals(2, postings.size)
        assertEquals("income:salary main job", postings[1].account)
        var balance = BigDecimal.ZERO
        for (posting in postings) {
            balance += posting.amount.quantity
            assertEquals("$", posting.amount.commodity)
        }
        assertEquals(BigDecimal.ZERO, balance)
        val lines = tx.toString().split("\n")
        assertEquals("2000-02-28 ! (#0431) Simple Transaction", lines[0])
        assertEquals("    assets:bank:checking                1\$", lines[1])
        assertEquals("    income:salary main job             -1\$", lines[2])
    }
}