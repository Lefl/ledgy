package one.apheli.common

import android.app.Application
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContracts
import one.apheli.common.model.Transaction

actual abstract class ViewModel : androidx.lifecycle.ViewModel() {
    public actual override fun onCleared() {
        super.onCleared()
    }
}