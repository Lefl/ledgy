package one.apheli.common.composable

import androidx.compose.material.AlertDialog
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape


@Composable
actual fun LAlertDialog(
    onDismissRequest: (() -> Unit),
    confirmButton: (@Composable () -> Unit),
    modifier: Modifier,
    dismissButton: (@Composable () -> Unit)?,
    title: (@Composable () -> Unit)?,
    text: (@Composable () -> Unit)?,
    shape: Shape,
    backgroundColor: Color,
    contentColor: Color,
) {
    AlertDialog(
        onDismissRequest,
        confirmButton,
        modifier,
        dismissButton,
        title,
        text,
        shape,
        backgroundColor,
        contentColor
    )
}
