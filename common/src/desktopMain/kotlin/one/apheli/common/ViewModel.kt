package one.apheli.common

actual abstract class ViewModel {
    actual open fun onCleared() {
        // NOOP
    }
}