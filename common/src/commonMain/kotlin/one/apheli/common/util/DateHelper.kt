package one.apheli.common.util

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DateHelper {
    companion object {
        fun parseDate(part: String): LocalDate {
            var dateSeparator = "/"
            if (part.contains("-")) {
                dateSeparator = "-"
            } else if (part.contains(".")) {
                dateSeparator = "."
            }
            val formatter = DateTimeFormatter.ofPattern("yyyy" + dateSeparator + "MM" + dateSeparator + "dd")
            return LocalDate.parse(part, formatter)
        }
    }
}

