package one.apheli.common.util

import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.ParseException
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class BigDecimalHelper {
    companion object {
        fun parseBigDecimal(amount: String, decimalMark: Char): BigDecimal {
            val format = getBigDecimalFormatter(decimalMark)
            // Find all numbers that are not enclosed in quotes.
            val quantityRegex = Regex(pattern = "[\\d-+ $decimalMark]+(?=([^\"]*\"[^\"]*\")*[^\"]*\$)")
            val quantityMatch = quantityRegex.find(amount)
            if (quantityMatch != null) {
                var priceStr = quantityMatch.value
                priceStr = priceStr.replace(" ", "")
                return format.parse(priceStr) as BigDecimal
            } else {
                throw ParseException("Missing price in posting", 0)
            }
        }

        private fun getBigDecimalFormatter(decimalMark: Char): DecimalFormat {
            val format = DecimalFormat()
            format.isParseBigDecimal = true
            val symbols = format.decimalFormatSymbols
            symbols.decimalSeparator = decimalMark
            symbols.groupingSeparator = if (decimalMark == '.') ',' else '.'
            return format
        }
    }
}

