package one.apheli.common.composable

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape


@Composable
expect fun LAlertDialog(
    onDismissRequest: (() -> Unit),
    confirmButton: (@Composable () -> Unit),
    modifier: Modifier = Modifier,
    dismissButton: (@Composable () -> Unit)?,
    title: (@Composable () -> Unit)?,
    text: (@Composable () -> Unit)?,
    shape: Shape,
    backgroundColor: Color,
    contentColor: Color,
)
