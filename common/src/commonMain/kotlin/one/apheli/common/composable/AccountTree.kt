package one.apheli.common.composable

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import one.apheli.common.model.Account

@Composable
fun AccountTree(account: Account?, collapsedAccounts: MutableList<String>, showAccountDetails: (String) -> Unit) {
    val accountScrollState = rememberScrollState()
    if (account != null) {
        Column(Modifier.fillMaxHeight().verticalScroll(accountScrollState)) {
            for (child in account.children) {
                AccountTree(child, 0, collapsedAccounts, showAccountDetails)
            }
        }
    }
}

@Composable
fun AccountTree(account: Account, level: Int, collapsedAccounts: MutableList<String>, showAccountDetails: (String) -> Unit) {
    if (level == 0) {
        Card(Modifier.padding(5.dp).fillMaxWidth(), elevation = 3.dp) {
            Account(account, level, collapsedAccounts, showAccountDetails)
        }
    } else {
        Account(account, level, collapsedAccounts, showAccountDetails)
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Account(account: Account, level: Int, collapsedAccounts: MutableList<String>, showAccountDetails: (String) -> Unit) {
    val haptic = LocalHapticFeedback.current
    var isClickable by remember { mutableStateOf(false) }
    val collapsedIdx = collapsedAccounts.indexOf(account.fullName)
    val iconRotation by animateFloatAsState(
        targetValue = if (collapsedIdx >= 0) -90f else 0f
    )
    val balances = account.getBranchBalances()
    if (balances.isEmpty()) {
        return
    }
    Column {
        var mod = Modifier.fillMaxWidth()
        if (account.children.isNotEmpty() || isClickable) {
            mod = mod.combinedClickable(
                onDoubleClick = { println("TODO: Switch to Transaction Tab with filter") },
                onLongClick = {
                    if (collapsedIdx >= 0) {
                        collapsedAccounts.removeAt(collapsedIdx)
                    } else {
                        collapsedAccounts.add(account.fullName)
                    }
                    haptic.performHapticFeedback(HapticFeedbackType.LongPress)
                },
                onClick = {
                    if (isClickable) {
                        showAccountDetails(account.fullName)
                    }
                }
            )
        }
        mod = mod.padding(start = (12 * level).dp, top = 5.dp, bottom = 5.dp)
        Row(modifier = mod, horizontalArrangement = Arrangement.SpaceBetween) {
            Row(modifier = Modifier.padding(end = 20.dp)) {
                var textPadding = 24.dp
                if (account.children.size > 0) {
                    Icon(Icons.Filled.KeyboardArrowDown, contentDescription = "Show", modifier = Modifier.rotate(iconRotation))
                    textPadding = 0.dp
                }
                Text(account.name, modifier = Modifier.padding(start = textPadding))
            }
            var first = true
            val text = buildAnnotatedString {
                for (commodity in balances) {
                    if (first) {
                        first = false
                    } else {
                        append(", ")
                    }
                    append(commodity.value.toString() + " " + commodity.key)
                }
            }
            Text(
                text = text,
                fontFamily = FontFamily.Monospace,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.padding(end = 5.dp),
                onTextLayout = { layout: TextLayoutResult ->
                    isClickable = layout.hasVisualOverflow
                }
            )
        }
        if (account.children.isNotEmpty()) {
            AnimatedVisibility(visible = collapsedIdx == -1) {
                Column {
                    for (child in account.children) {
                        AccountTree(child, level + 1, collapsedAccounts, showAccountDetails)
                    }
                    if (level > 0) {
                        Divider()
                    }
                }
            }
        }
    }
}
