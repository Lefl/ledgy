package one.apheli.common.composable

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import one.apheli.common.model.Account
import java.math.BigDecimal

@Composable
fun AccountList(accountList: List<Account>) {
    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        items(accountList) {
            AccountListItem(it)
        }
    }
}

@Composable
fun AccountListItem(account: Account) {
    var isExpanded by remember { mutableStateOf(false) }
    val rotationState by animateFloatAsState(
        targetValue = if (isExpanded) 180f else 0f
    )
    Card(modifier = Modifier.padding(5.dp).clickable { isExpanded = !isExpanded} , elevation = 3.dp) {
        Column {
            var first = true
            for (pair in account.balances) {
                if (first) {
                    AccountListItemContent(account.fullName, pair.key, pair.value, true)
                } else {
                    AnimatedVisibility(visible = isExpanded) {
                        AccountListItemContent(account.fullName, pair.key, pair.value, false)
                    }
                }
                first = false
            }
            if (account.balances.size > 1) {
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                    Icon(
                        Icons.Filled.KeyboardArrowDown,
                        modifier = Modifier.rotate(rotationState),
                        contentDescription = "More",
                        tint = Color(0f, 0f, 0f, 0.5f)
                    )
                }
            }
        }
    }
}

@Composable
fun AccountListItemContent(accountName: String, commodity: String, quantity: BigDecimal, first: Boolean) {
    Row(
        modifier = Modifier.fillMaxWidth().padding(5.dp),
        horizontalArrangement = if (first) Arrangement.SpaceBetween else Arrangement.End
    ) {
        if (first) {
            Text(accountName)
        }
        val color = if (quantity < BigDecimal.ZERO) Color.Red else Color.Black
        Text(
            text = quantity.toString() + commodity,
            fontFamily = FontFamily.Monospace,
            color = color
        )
    }
}
