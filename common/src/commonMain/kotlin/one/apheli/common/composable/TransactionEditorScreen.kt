package one.apheli.common.composable

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import one.apheli.common.model.Amount
import one.apheli.common.model.Posting
import one.apheli.common.model.Transaction
import one.apheli.common.navigation.Navigator
import one.apheli.common.navigation.Screen
import one.apheli.common.util.BigDecimalHelper
import one.apheli.common.util.DateHelper
import java.math.BigDecimal
import java.text.ParseException
import java.time.LocalDate
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.ui.graphics.Color
import one.apheli.common.LedgyViewModel
import java.io.OutputStream
import java.time.format.DateTimeFormatter

data class TransactionEditorScreen(
    val viewModel: LedgyViewModel,
    val onEditTransactionChange: (Transaction) -> Unit,
    val onEditPostingChange: (Int, Posting) -> Unit,
    val addEditPosting: (Posting) -> Unit,
    val prepareOutputStream: (LedgyViewModel) -> OutputStream?
) : Screen {

    @Composable
    override fun Content(navigator: Navigator) {
        val editTransaction = viewModel.transactions.last()
        var dateText by remember { mutableStateOf(editTransaction.date.format(DateTimeFormatter.ISO_LOCAL_DATE)) }
        var dateError by remember { mutableStateOf(false) }
        val scrollState = rememberScrollState()
        Scaffold(
            topBar = {
                TopAppBar(
                    navigationIcon = {
                        IconButton(onClick = { viewModel.removeEditTransaction(); navigator.onBackPressed() }) {
                            Icon(Icons.Filled.ArrowBack, contentDescription = "Back")
                        }
                    },
                    title = { Text("Edit Transaction") },
                    actions = {
                        IconButton(onClick = {
                            if (viewModel.editValid) {
                                println("OH NO")
                                viewModel.saveTransaction(prepareOutputStream); navigator.onBackPressed()
                            }
                        }) {
                            val alpha = if (viewModel.editValid) 1.0f else 0.40f
                            Icon(
                                Icons.Filled.Check,
                                tint = Color(1f, 1f, 1f, alpha),
                                contentDescription = "Save"
                            )
                        }
                    }
                )
            },
            content = {
                Column(Modifier.padding(5.dp).verticalScroll(scrollState)) {
                    Row(Modifier.fillMaxWidth()) {
                        OutlinedTextField(
                            modifier = Modifier.width(130.dp).padding(end = 10.dp),
                            singleLine = true,
                            isError = dateError,
                            value = dateText,
                            onValueChange = {
                                dateText = it
                                dateError =
                                    !(it.isEmpty() || it.matches("^[0-9]{4}[.\\-\\/][0-9]{2}[.\\-\\/][0-9]{2}\$".toRegex()))
                                if (!dateError) {
                                    val date: LocalDate?
                                    try {
                                        date = DateHelper.parseDate(dateText)
                                        onEditTransactionChange(editTransaction.copy(date = date))
                                    } catch (_: IllegalArgumentException) {
                                        dateError = true
                                    }
                                } else {
                                    dateError = true
                                }
                                onEditTransactionChange(editTransaction.copy(dateError = dateError))
                            },
                            label = { Text("Date") }
                        )
                        OutlinedTextField(
                            modifier = Modifier.fillMaxWidth(),
                            singleLine = true,
                            value = editTransaction.description,
                            onValueChange = { onEditTransactionChange(editTransaction.copy(description = it)) },
                            label = { Text("Description") }
                        )
                    }
                    Divider(modifier = Modifier.padding(top = 5.dp))
                    for ((index, pst) in editTransaction.postings.withIndex()) {
                        EditPosting(index, pst, onEditPostingChange)
                    }
                    OutlinedButton(
                        onClick = { addEditPosting(Posting("", Amount(BigDecimal.ZERO, ""))) },
                    ) {
                        Text("Add Posting")
                    }
                }
            }
        )
    }

    @Composable
    private fun EditPosting(
        index: Int,
        pst: Posting,
        onEditPostingChange: (Int, Posting) -> Unit
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            OutlinedTextField(
                modifier = Modifier.padding(end = 10.dp).weight(3f),
                isError = pst.account.isBlank() && (pst.amount.editQuantity.isNotBlank() || pst.amount.commodity.isNotBlank()),
                singleLine = true,
                value = pst.account,
                onValueChange = {
                    onEditPostingChange(index, pst.copy(account = it))
                },
                label = { Text("Account ${index + 1}") }
            )
            OutlinedTextField(
                modifier = Modifier.padding(end = 10.dp).weight(2f, true),
                isError = pst.amount.editQuantity.isNotEmpty() && pst.amount.quantityError,
                singleLine = true,
                textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.End, fontFamily = FontFamily.Monospace),
                value = pst.amount.editQuantity,
                onValueChange = {
                    if (it.isBlank()) {
                        pst.amount.quantityError = false
                    } else if (it.matches("^[0-9 ,.+\\-]*\$".toRegex())) {
                        try {
                            pst.amount.quantity = BigDecimalHelper.parseBigDecimal(it, '.')
                            pst.amount.quantityError = false
                        } catch (_: ParseException) {
                            pst.amount.quantityError = true
                        }
                    } else {
                        pst.amount.quantityError = true
                    }
                    onEditPostingChange(index, pst.copy(amount = pst.amount.copy(editQuantity = it)))
                },
                label = { Text("QTY") }
            )
            OutlinedTextField(
                modifier = Modifier.width(75.dp).weight(1f, true),
                textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.Start, fontFamily = FontFamily.Monospace),
                singleLine = true,
                value = pst.amount.commodity,
                onValueChange = {
                    onEditPostingChange(index, pst.copy(amount = pst.amount.copy(commodity = it)))
                },
                label = { Text("¤") },
            )
        }
    }
}
