package one.apheli.common.composable

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import one.apheli.common.model.Posting
import one.apheli.common.model.Transaction
import java.math.BigDecimal

@Composable
fun Transaction(transaction: Transaction, maxPostings: Int) {
    var isExpanded by remember { mutableStateOf(false) }
    val rotationState by animateFloatAsState(
        targetValue = if (isExpanded) 180f else 0f
    )
    Card(modifier = Modifier.padding(5.dp).clickable { isExpanded = !isExpanded }, elevation = 3.dp) {
        Column {
            Row(modifier = Modifier.fillMaxWidth().padding(5.dp), horizontalArrangement = Arrangement.SpaceBetween) {
                Text(transaction.date.toString())
                Text(transaction.description)
            }
            Divider()
            val cutOff = transaction.postings.size > maxPostings + 1
            for ((index, pst) in transaction.postings.withIndex()) {
                if (index < 2 || !cutOff) {
                    Posting(pst)
                } else {
                    AnimatedVisibility(visible = isExpanded) {
                        Posting(pst)
                    }
                }
            }
            if (cutOff) {
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                    Icon(
                        Icons.Filled.KeyboardArrowDown,
                        modifier = Modifier.rotate(rotationState),
                        contentDescription = "More",
                        tint = Color(0f, 0f, 0f, 0.5f)
                    )
                }
            }
        }
    }
}

@Composable
fun Posting(posting: Posting) {
    val color = if (posting.amount.quantity < BigDecimal.ZERO) Color.Red else Color.Black
    Row(modifier = Modifier.fillMaxWidth().padding(5.dp), horizontalArrangement = Arrangement.SpaceBetween) {
        Text(posting.shortAccount)
        Text(
            text = posting.amount.quantity.toString() + " " + posting.amount.commodity, color = color,
            fontFamily = FontFamily.Monospace
        )
    }
}
