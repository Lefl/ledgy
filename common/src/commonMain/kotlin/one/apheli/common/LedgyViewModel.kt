package one.apheli.common

import androidx.compose.runtime.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import one.apheli.common.model.*
import java.io.InputStream
import java.io.OutputStream
import java.math.BigDecimal
import java.time.LocalDate
import java.util.TreeMap


class LedgyViewModel() : ViewModel() {
    private var journal: Journal? = null
    var hasJournal = false
        private set

    var accounts = mutableStateListOf<Account>()
        private set
    var transactions = mutableStateListOf<Transaction>()
        private set
    var groupedTransactions = mapOf<String, List<Transaction>>()
        private set
    var isLoading by mutableStateOf(false)
    val journalPath: String
        get() = if (journal != null) journal!!.path else ""
    private var editPostings = mutableStateListOf<Posting>()
    var editValid by mutableStateOf(false)
    var collapsedAccounts = mutableStateListOf<String>()
    var detailedAccount by mutableStateOf("")
        private set

    fun getAccountTree(): Account? { return journal?.accountTree }
    fun showAccountDetails(account: String) {
        println("New account $account")
        detailedAccount = account
    }
    fun closeAccountDetails() {
        println("Hiding account details")
        detailedAccount = ""
    }

    fun newEditTransaction() {
        editValid = false
        editPostings = mutableStateListOf(Posting("", Amount(BigDecimal.ZERO, "")))
        editPostings.add(Posting("", Amount(BigDecimal.ZERO, "")))
        val tx = Transaction(
            transactions.size,
            LocalDate.now(),
            code = "",
            description = "",
            postings = editPostings,
        )
        transactions.add(tx)
    }
    fun removeEditTransaction() {
        editPostings = mutableStateListOf()
        transactions.removeLast()
    }
    fun onEditTransactionChange(tx: Transaction) {
        println(tx)
        if (transactions[transactions.size - 1].description == tx.description) {
            editValid = tx.isValid()
            println(editValid)
        }
        transactions[transactions.size - 1] = tx
    }
    fun onEditPostingChange(index: Int, pst: Posting) {
        editPostings[index] = pst
        editValid = transactions[transactions.size - 1].isValid()
    }
    fun addEditPosting(pst: Posting) {
        editPostings.add(pst)
    }
    fun saveTransaction(streamOpener: (LedgyViewModel) -> OutputStream?) {
        println("Start saving Transaction")
        val tx = transactions.last()
        tx.calculateImpliedPostings()
        // Update Journal accounts using posting accounts
        for (pst in tx.postings) {
            val balances = journal!!.accountTree.getAccount(pst.account).balances
            balances[pst.amount.commodity] =
                balances.getOrDefault(pst.amount.commodity, BigDecimal.ZERO) + pst.amount.quantity
        }
        println("Updated AccountTree")
        journal!!.transactions.add(tx)
        journal?.accountList = journal?.accountTree!!.toList()
        accounts.clear()
        accounts.addAll(journal?.accountList!!)

        println("Updated Journal")

        val stream = streamOpener(this)
        if (stream != null) {
            editPostings = mutableStateListOf()
            println("Cleared EditPosting")
            transactions.removeLast()
            transactions.add(tx)
            groupedTransactions = TreeMap(transactions.groupBy { it.date.monthValue.toString() + "-" + it.date.year })
            println("Readded transaction to state list")
            writeTransaction(stream, tx)
        }
    }

    fun importFile(stream: InputStream, path: String) {
        isLoading = true
        CoroutineScope(Dispatchers.IO).launch {
            journal = importJournal(stream, path)
            accounts = journal?.accountList!!.toMutableStateList()
            transactions = journal?.transactions!!.toMutableStateList()
            groupedTransactions = TreeMap(transactions.groupBy { it.date.monthValue.toString().padStart(2, '0') + "-" + it.date.year })
            hasJournal = true
            isLoading = false
        }
    }


    fun writeTransaction(stream: OutputStream, transaction: Transaction) {
        isLoading = true
        CoroutineScope(Dispatchers.IO).launch {
            stream.bufferedWriter().use {
                it.newLine()
                it.newLine()
                it.write(transaction.toString())
                println("Done writing Transaction")
            }
            println("Written Transaction")
            isLoading = false
        }
    }
}
