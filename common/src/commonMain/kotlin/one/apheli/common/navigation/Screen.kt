package one.apheli.common.navigation

import androidx.compose.runtime.Composable

interface Screen {
    @Composable
    fun Content(navigator: Navigator)
}