package one.apheli.common.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf

class Navigator {
    private val backStack = mutableStateListOf<Screen>()

    fun onBackPressed(): Boolean {
        if (backStack.size <= 1) {
            return false
        }
        backStack.removeLast()
        return true
    }

    fun show(screen: Screen) {
        backStack.add(screen)
    }

    @Composable
    fun host() {
        if (backStack.isEmpty()) {
            return
        }
        backStack.last().Content(this)
    }

    fun size(): Int {
        return backStack.size
    }
}