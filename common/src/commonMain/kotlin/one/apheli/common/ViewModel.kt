package one.apheli.common

import one.apheli.common.model.Transaction


expect abstract class ViewModel() {
    open fun onCleared()
}