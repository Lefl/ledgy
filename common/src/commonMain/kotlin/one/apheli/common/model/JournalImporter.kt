package one.apheli.common.model

import one.apheli.common.util.BigDecimalHelper
import one.apheli.common.util.DateHelper
import java.io.InputStream
import java.math.BigDecimal
import java.text.ParseException

fun importJournal(stream: InputStream, path: String): Journal {
    val startTime = System.currentTimeMillis()
    // Matches a date, plus a whitespace and other characters
    val dateRegex = Regex(pattern = "([0-9]{4}(\\/|.|-))?[0-9]{1,2}(\\/|.|-)[0-9]{1,2}\\s.*")
    // Matches a line that starts with any amount of whitespace + chars without the first char being a ";"
    val postingRegex = Regex(pattern = "^\\s+(?!.*;).*")

    val journal = Journal(path = path)

    var currentTransactionIdx = 0
    var currentTransaction: Transaction? = null
    var currentLine = 0
    stream.bufferedReader().forEachLine {
        currentLine++
        if (it.startsWith("decimal-mark")) {
            val mark = it.split(" ")[1]
            journal.decimalMark = mark[0]
        } else if (dateRegex.matches(it)) {
            // Found Transaction!
            currentTransaction = parseTransaction(it, currentTransactionIdx++, currentLine)
        } else if (postingRegex.matches(it)) {
            if (currentTransaction == null) {
                // Found a posting without a transaction
                throw ParseException("Found stray posting at line $currentLine", currentLine)
            } else {
                val posting = parsePosting(it, journal.decimalMark, currentLine)
                currentTransaction!!.postings.add(posting)
                // Add posting balance to account.
                val balances = journal.accountTree.getAccount(posting.account).balances
                balances[posting.amount.commodity] =
                    balances.getOrDefault(posting.amount.commodity, BigDecimal.ZERO) + posting.amount.quantity
                // balances[posting.amount.priceCommodity] = balances.getOrDefault(posting.amount.commodity, BigDecimal.ZERO) + posting.amount.quantity
            }
        } else if (it.trim().isEmpty()) {
            if (currentTransaction != null) {
                journal.transactions.add(currentTransaction!!)
            }
            currentTransaction = null
        }
    }
    if (currentTransaction != null) {
        journal.transactions.add(currentTransaction!!)
    }
    journal.accountList = journal.accountTree.toList()
    journal.commodities = journal.accountTree.getCommodities()
    println("Imported Journal \"$path\" in ${System.currentTimeMillis() - startTime} milliseconds")
    return journal
}

private fun parseTransaction(line: String, index: Int, lineNr: Int): Transaction {
    val parts = line.split("\\s+".toRegex())
    var partIndex = 0
    val date = DateHelper.parseDate(parts[partIndex++])
    val status = parseStatus(parts[partIndex])
    partIndex = if (status != Status.NONE) partIndex + 1 else partIndex
    val code = parseCode(parts[partIndex])
    partIndex = if (code.isNotEmpty()) partIndex + 1 else partIndex
    val description = parts.subList(partIndex, parts.size).joinToString(" ")
    return Transaction(
        index = index,
        date = date,
        status = status,
        code = code,
        description = description
    )
}

private fun parseStatus(part: String?): Status {
    var status = Status.NONE
    if (part == "!" || part == "*") {
        status = if (part == "!") Status.PENDING else Status.CLEARED
    }
    return status
}

private fun parseCode(part: String): String {
    var code = ""
    if (Regex("\\(.*\\)").matches(part)) {
        code = part.substring(1, part.length - 1)
    }
    return code
}

private fun parsePosting(line: String, decimalMark: Char, lineNr: Int): Posting {
    // Insane regex, maybe redo this?
    val postingRegex = Regex(pattern = "\\s+(?<status>[!|*] )?(?<account>\\S+(?: \\S+)*(?=\\s+?))[ ]{2,}(?<amount>\\S+(?: \\S+)*)(?<comment> *;.*)?")
    val groups = postingRegex.matchEntire(line)?.groups
    if (groups != null) {
        try {
            // First capture group is the whole line...
            val status = parseStatus(groups[1]?.value?.trim())
            val account = groups[2]!!.value
            val amount = parseAmount(groups[3]!!.value, decimalMark)
            val comment = if (groups[4] != null) groups[4]!!.value else ""
            return Posting(
                account = account,
                amount = amount,
                comment = comment,
                status = status
            )
        } catch(e: NullPointerException) {
            throw ParseException("Could not parse Posting", lineNr)
        }
    }
    throw ParseException("Could not parse Posting", lineNr)
}

private fun parseAmount(amount: String, decimalMark: Char): Amount {
    val splitAmount = amount.split("@{1,2}".toRegex())

    val priceTypeRegex = Regex("@{2}(?=([^\"]*\"[^\"]*\")*[^\"]*\$)")
    val isUnitPrice = !priceTypeRegex.matches(amount)

    val quantity = BigDecimalHelper.parseBigDecimal(splitAmount[0], decimalMark)
    val commodity = parseCommodity(splitAmount[0])

    var price = BigDecimal(0)
    var priceCommodity = ""
    if (splitAmount.size > 1) {
        price = BigDecimalHelper.parseBigDecimal(splitAmount[1], decimalMark)
        priceCommodity = parseCommodity(splitAmount[1])
    }

    return Amount(
        quantity = quantity,
        commodity = commodity,
        price = price,
        priceCommodity = priceCommodity,
        unitPrice = isUnitPrice
    )
}


private fun parseCommodity(amount: String): String {
    val commodityRegex = Regex(pattern = "([A-za-z\\p{Sc}]+|(\\\".*?\\\")+)")
    val commodityMatch = commodityRegex.find(amount)
    return commodityMatch?.value ?: ""
}
