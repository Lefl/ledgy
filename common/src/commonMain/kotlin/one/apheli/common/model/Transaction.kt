package one.apheli.common.model

import java.math.BigDecimal
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

data class Transaction(
    var index: Int, // Place in the Journal file
    var date: LocalDate,
    var code: String,
    var description: String,
    var status: Status = Status.NONE,
    var secondaryDate: LocalDate? = null,
    var changed: Boolean = false, // Did the value change after it has been read from the journal file?
    var postings: MutableList<Posting> = mutableListOf(),
    var tags: HashMap<String, String> = hashMapOf(),
    var dateError: Boolean = false
) {
    fun isValid(): Boolean {
        println("Running!")
        var emptyQtys = 0
        if (dateError) {
            return false
        }
        println("Checking postings")
        val qtyMap = mutableMapOf<String, BigDecimal>()
        for (posting in postings) {
            println(posting)
            val amount = posting.amount
            // Check if there's an error in the quantity or the price
            if (amount.quantityError || amount.priceError) {
                println("QTY ERROR")
                return false
            }
            // Check if the account is empty but everything else is not
            if (posting.account.isBlank() && (amount.editQuantity.isNotBlank() || amount.commodity.isNotBlank())) {
                println("ACCOUNT BLANK")
                return false
            }
            // Check if there are multiple postings where the quantity is empty
            if (!posting.isEmpty() && amount.editQuantity.isBlank()) {
                emptyQtys++
                if (emptyQtys > 1) {
                    println("EMPTY POSTINGS")
                    return false
                }
            }
            qtyMap[amount.commodity] = qtyMap.getOrDefault(amount.commodity, BigDecimal.ZERO) + amount.quantity
        }
        // Check if transaction is balanceable
        println(qtyMap)
        println(emptyQtys)
        if (qtyMap.count { it.value != BigDecimal.ZERO } > 0 && emptyQtys == 0) {
            return false
        }

        return true
    }
    fun calculateImpliedPostings() {
        postings.removeIf { it.implied }
        // Calculate implied posting, if it exists
        var blankPosting: Posting? = null
        val qtyMap = mutableMapOf<String, BigDecimal>()
        for (pst in postings) {
            if (pst.amount.editQuantity.isEmpty()) {
                blankPosting = pst
            } else {
                qtyMap[pst.amount.commodity] = qtyMap.getOrDefault(pst.amount.commodity, BigDecimal.ZERO) + pst.amount.quantity
            }
        }
        val unbalanced = qtyMap.filter { it.value != BigDecimal.ZERO }
        if (blankPosting != null && unbalanced.isNotEmpty()) {
            for (entry in unbalanced) {
                val amnt = Amount(
                    quantity = BigDecimal.ZERO - entry.value,
                    commodity = entry.key,
                    implied = true
                )
                postings.add(
                    Posting(
                        account = blankPosting.account,
                        amount = amnt,
                        implied = true
                    )
                )
            }
        }
    }


    override fun toString(): String {
        return buildString {
            append(date.format(DateTimeFormatter.ISO_LOCAL_DATE))
            when(status) {
                Status.CLEARED -> append(" * ")
                Status.PENDING -> append(" ! ")
                Status.NONE -> append(" ")
            }
            if (code.isNotEmpty()) {
                append("($code) ")
            }
            append(description)
            for (pst in postings) {
                append("\n")
                append(pst.toString())
            }
        }
    }
}
