package one.apheli.common.model

enum class Status {
    NONE,
    PENDING,
    CLEARED
}