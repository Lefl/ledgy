package one.apheli.common.model

import java.math.BigDecimal
import java.time.LocalDate

data class Posting(
    var account: String,
    val amount: Amount,
    val comment: String = "",
    val status: Status = Status.NONE,
    val date: LocalDate? = null,
    val virtual: Boolean = false,
    val tags: HashMap<String, String> = hashMapOf(),
    val implied: Boolean = false
) {
    val shortAccount = buildShortAccount()

    fun isEmpty(): Boolean {
        return account.isBlank() && amount.editQuantity.isBlank() && amount.editPrice.isBlank()
    }

    private fun buildShortAccount(): String {
        var acc = ""
        val splitAccount = account.split(":")
        for ((index, name) in splitAccount.withIndex()) {
            acc += if (index > 0) ":" else ""
            if (index < splitAccount.size - 1) {
                acc += name.substring(0, 2)
            } else {
                acc += name
            }
        }
        return acc
    }

    override fun toString(): String {
        return buildString {
            append("    ")
            when(status) {
                Status.CLEARED -> append("* ")
                Status.PENDING -> append("! ")
            }
            append(account)
            if (!amount.implied) {
                val quantity = amount.quantity.toString()
                var spaces = 38 - account.length - amount.commodity.length - (quantity.length)
                spaces -= if (amount.commodity.length > 3) 1 else 0
                spaces -= if (status != Status.NONE) 2 else 0
                spaces = if (spaces <= 0) 2 else spaces
                append("".padEnd(spaces, ' '))

                append(quantity)
                if (amount.commodity.length > 3) {
                    append(" ")
                }
                append(amount.commodity)
                if (amount.price != BigDecimal.ZERO) {
                    if (amount.unitPrice) append(" @ ") else append(" @@ ")
                    append(amount.price)
                    append(amount.priceCommodity)
                    if (amount.priceCommodity.length > 3) {
                        append(" ")
                    }
                }
            }
        }
    }
}