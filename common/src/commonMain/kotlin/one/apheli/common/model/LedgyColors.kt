package one.apheli.common.model

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

class LedgyColors {
    companion object {
        val LightColors = lightColors(
            primary = Color(0, 89, 165),
            primaryVariant = Color(0, 46, 106)
        )
        val DarkColors = darkColors(
            primary = Color(0, 89, 165),
            primaryVariant = Color(0, 46, 106)
        )
    }
}