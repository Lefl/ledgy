package one.apheli.common.model

import java.math.BigDecimal

class Account(val name: String, val fullName: String, val parent: Account? = null) {
    val children = mutableListOf<Account>()
    var balances = HashMap<String, BigDecimal>()

    fun getBranchBalances(): HashMap<String, BigDecimal> {
        val map = hashMapOf<String, BigDecimal>()
        fillBranchBalances(map)
        return map
    }
    private fun fillBranchBalances(map: HashMap<String, BigDecimal>) {
        for (commodity in balances) {
            map[commodity.key] = map.getOrDefault(commodity.key, BigDecimal.ZERO) + commodity.value
        }
        for (account in children) {
            account.fillBranchBalances(map)
        }
    }

    fun toList(): List<Account> {
        var list = listOf<Account>(this)
        for (child in children) {
            list = list.union(child.toList()).filter { it.balances.isNotEmpty() }.toList()
        }
        return list
    }

    fun getCommodities(): List<String> {
        var list = this.balances.keys as Set<String>
        for (child in children) {
            list = list.union(child.getCommodities())
        }
        return list.toList()
    }

    fun getAccount(path: String): Account {
        return getAccount(path.split(":").toMutableList())
    }

    private fun getAccount(path: MutableList<String>): Account {
        val nextAccount = path.removeFirst()
        for (child in children) {
            if (child.name == nextAccount) {
                return if (path.isEmpty()) child else child.getAccount(path)
            }
        }
        // Couldn't find account, creating new one...
        var nextFullName = ""
        if (fullName != "root") {
            nextFullName += "$fullName:"
        }
        nextFullName += nextAccount
        val newAccount = Account(nextAccount, nextFullName, this)
        children.add(newAccount)
        children.sortBy { it.name }
        return if (path.isEmpty()) newAccount else newAccount.getAccount(path)
    }
}