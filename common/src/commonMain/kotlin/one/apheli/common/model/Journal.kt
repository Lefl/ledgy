package one.apheli.common.model

import java.time.Year

data class Journal(
    val path: String,
    val defaultYear: Int = Year.now().value,
    var decimalMark: Char = '.',
    var accountTree: Account = Account("root", "root"),
    var accountList: List<Account> = listOf(),
    var commodities: List<String> = listOf(),
    var transactions: MutableList<Transaction> = mutableListOf(),
    var includedJournals: MutableList<Journal> = mutableListOf()
)
