package one.apheli.common.model

import java.math.BigDecimal

data class Amount(
    var quantity: BigDecimal,
    val commodity: String,
    val price: BigDecimal = BigDecimal.ZERO,
    val priceCommodity: String = "",
    val unitPrice: Boolean = true,
    var implied: Boolean = false,
    /* Used as state for edit fields */
    var editQuantity: String = if (quantity == BigDecimal.ZERO) "" else quantity.toString(),
    var quantityError: Boolean = false,
    var editPrice: String = if (price == BigDecimal.ZERO) "" else price.toString(),
    var priceError: Boolean = false,
)
