package one.apheli.common

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import one.apheli.common.composable.*
import one.apheli.common.model.Transaction
import one.apheli.common.navigation.Navigator
import one.apheli.common.navigation.Screen
import java.io.OutputStream


data class MainScreen(
    val viewModel: LedgyViewModel,
    val fileChooser: () -> Unit,
    val prepareOutputStream: (LedgyViewModel) -> OutputStream?
    ) : Screen {

    //var tabState by mutableStateOf(0)

    @Composable
    override fun Content(navigator: Navigator) {
        val maxPostings = 2
        var tabState by remember { mutableStateOf(0) }
        val tabTitles = listOf("Accounts", "Transactions")
        var contextMenuState by remember { mutableStateOf(false) }
        var searchState by rememberSaveable { mutableStateOf(false) }
        var searchText by rememberSaveable { mutableStateOf("") }
        var accountViewIsList by remember { mutableStateOf(false) }

        val scrollState = rememberScrollState()
        val coroutineScope = rememberCoroutineScope()

        Scaffold(
            backgroundColor = MaterialTheme.colors.background,
            floatingActionButton = {
                if (tabState == 1 && viewModel.hasJournal) {
                    FloatingActionButton(onClick = {
                        viewModel.newEditTransaction()
                        navigator.show(TransactionEditorScreen(
                            viewModel,
                            viewModel::onEditTransactionChange,
                            viewModel::onEditPostingChange,
                            viewModel::addEditPosting,
                            prepareOutputStream
                        ))
                    }) {
                        Icon(Icons.Filled.Add, contentDescription = "Add Transaction")
                    }
                }
            },
            topBar = {
                Column {
                    TopAppBar(
                        title = {
                            if (searchState) {
                                TextField(
                                    value = searchText,
                                    onValueChange = { newText -> searchText = newText },
                                    singleLine = true,
                                    modifier = Modifier.fillMaxWidth(),
                                    placeholder = { Text("Search") }
                                )
                            } else {
                                Text("Ledgy")
                            }
                        },
                        actions = {
                            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.high) {
                                if (searchState) {
                                    IconButton(onClick = { searchState = false }) {
                                        Icon(Icons.Filled.Close, contentDescription = "Cancel")
                                    }
                                } else {
                                    AnimatedVisibility(visible = tabState == 1) {
                                        IconButton(onClick = { searchState = true }) {
                                            Icon(Icons.Filled.Search, contentDescription = "Search")
                                        }
                                    }
                                    IconButton(onClick = { TODO("Not yet implemented") }) {
                                        Icon(Icons.Filled.DateRange, contentDescription = "Period")
                                    }
                                }
                                Box {
                                    IconButton(onClick = { contextMenuState = true }) {
                                        Icon(Icons.Filled.MoreVert, contentDescription = "Menu")
                                    }
                                    DropdownMenu(
                                        expanded = contextMenuState,
                                        onDismissRequest = { contextMenuState = false },
                                        modifier = Modifier,
                                        content = {
                                            DropdownMenuItem(onClick = {
                                                fileChooser(); contextMenuState = false
                                            }) {
                                                Text("Open")
                                            }
                                            if (tabState == 0) {
                                                DropdownMenuItem(onClick = {
                                                    accountViewIsList = !accountViewIsList; contextMenuState =
                                                    false
                                                }) {
                                                    Checkbox(
                                                        checked = accountViewIsList,
                                                        modifier = Modifier.padding(end = 5.dp),
                                                        onCheckedChange = null
                                                    )
                                                    Text("List")
                                                }
                                            }
                                        }
                                    )
                                }
                            }
                        }
                    )
                    TabRow(selectedTabIndex = tabState) {
                        tabTitles.forEachIndexed { index, title ->
                            Tab(
                                text = { Text(title) },
                                selected = tabState == index,
                                onClick = { tabState = index; coroutineScope.scroll(scrollState, index) }
                            )
                        }
                    }
                }
            },
            content = {
                if (viewModel.detailedAccount.isNotEmpty()) {
                    val account = viewModel.getAccountTree()?.getAccount(viewModel.detailedAccount)
                    LedgyAlertDialog(
                        modifier = Modifier.requiredWidth(250.dp),
                        title = { Text(account?.fullName ?: "", style = MaterialTheme.typography.h6) },
                        text = {
                            Column {
                                for (balance in account!!.getBranchBalances()) {
                                    Row {
                                        Text(
                                            text = "${balance.value} ${balance.key}",
                                            style = MaterialTheme.typography.body1,
                                            fontFamily = FontFamily.Monospace,
                                            overflow = TextOverflow.Visible
                                        )
                                    }
                                }
                            }
                        },
                        onDismissRequest = { viewModel.closeAccountDetails() },
                        confirmButton = {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                OutlinedButton(onClick = { viewModel.closeAccountDetails() }) {
                                    Text("Close")
                                }
                            }
                        },
                        dismissButton = {}
                    )
                }
                BoxWithConstraints {
                    val width = maxWidth
                    Row(modifier = Modifier.horizontalScroll(scrollState)) {
                        Box(Modifier.requiredWidth(width = width)) {
                            if (accountViewIsList) {
                                AccountList(viewModel.accounts)
                            } else {
                                AccountTree(
                                    viewModel.getAccountTree(),
                                    viewModel.collapsedAccounts,
                                    viewModel::showAccountDetails
                                )
                            }
                        }
                        Box(Modifier.requiredWidth(width = width)) {
                            TransactionTabContent(viewModel.groupedTransactions, maxPostings)
                        }
                        if (!scrollState.isScrollInProgress) {
                            val pos = findPosition(scrollState)
                            tabState = pos
                            if (scrollState.value != 0 && scrollState.value != scrollState.maxValue) {
                                coroutineScope.scroll(scrollState, pos)
                            }
                        }
                    }
                }
                if (viewModel.isLoading) {
                    Box(modifier = Modifier.fillMaxWidth().padding(top = 10.dp), contentAlignment = Alignment.Center) {
                        Card(shape = RoundedCornerShape(50), elevation = 20.dp) {
                            CircularProgressIndicator(Modifier.size(40.dp).padding(10.dp), strokeWidth = 3.dp)
                        }
                    }
                }
            }
        )
    }


    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    private fun TransactionTabContent(groupedTransactions: Map<String, List<Transaction>>, maxPostings: Int) {
        LazyColumn {
            groupedTransactions.keys.reversed().forEach { date ->
                stickyHeader {
                    TransactionGroupHeader(date)
                }

                items(groupedTransactions[date]!!.reversed()) { tx ->
                    Transaction(tx, maxPostings)
                }
            }
        }
    }

    @Composable
    private fun TransactionGroupHeader(date: String) {
        val mod = Modifier.fillMaxWidth().background(Color(240, 240, 240)).padding(top = 5.dp)
        Row(modifier = mod) {
            Column {
                Text(date, modifier = Modifier.padding(10.dp, 2.dp), style = MaterialTheme.typography.h6)
                Divider()
            }
        }
    }

    private fun CoroutineScope.scroll(scrollState: ScrollState, position: Int) {
        launch {
            scrollState.animateScrollTo(if (position == 0) 0 else scrollState.maxValue)
        }
    }

    private fun findPosition(scrollState: ScrollState): Int {
        return if (scrollState.value < scrollState.maxValue / 2) {
            0
        } else {
            1
        }
   }
}
