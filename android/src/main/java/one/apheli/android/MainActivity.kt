package one.apheli.android

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.graphics.toArgb
import one.apheli.common.LedgyViewModel
import one.apheli.common.MainScreen
import one.apheli.common.model.LedgyColors
import one.apheli.common.navigation.Navigator
import java.io.OutputStream

class MainActivity : AppCompatActivity() {
    companion object {
        const val JOURNAL_URI = "journalUri"
    }
    private val viewModel by viewModels<LedgyViewModel>()
    private val navigator = Navigator()
    override fun onCreate(savedInstanceState: Bundle?) {
        val previousJournal = loadJournalUri()
        println("Previous journal $previousJournal")
        if (previousJournal != null) {
            val stream = contentResolver.openInputStream(previousJournal)
            if (stream != null) {
                viewModel.importFile(stream, previousJournal.toString())
            }
        }

        super.onCreate(savedInstanceState)
        setContent {
            LaunchedEffect(Unit) {
                navigator.show(MainScreen(viewModel, ::startFilechooser, ::prepareOutputStream))
            }
            MaterialTheme(
                colors = if(isSystemInDarkTheme()) LedgyColors.DarkColors else LedgyColors.LightColors
            ) {
                window.statusBarColor = MaterialTheme.colors.primaryVariant.toArgb()
                navigator.host()
            }
        }
    }

    private val startForResult = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
        val takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        if (it != null) {
            contentResolver.takePersistableUriPermission(it, takeFlags)
            saveJournalUri(it)
            val stream = contentResolver.openInputStream(it)
            if (stream != null) {
                viewModel.importFile(stream, it.toString())
            }
        }
    }

    private fun startFilechooser() {
        startForResult.launch(arrayOf("application/octet-stream"))
    }

    private fun prepareOutputStream(viewModel: LedgyViewModel): OutputStream? {
        val uri = Uri.parse(viewModel.journalPath)
        return if (uri != null) {
            contentResolver.openOutputStream(uri)
        } else {
            null
        }
    }

    private fun saveJournalUri(uri: Uri) {
        println("Saving Journal Uri!")
        val pref = getPreferences(Context.MODE_PRIVATE)
        with(pref.edit()) {
            putString(JOURNAL_URI, uri.toString())
            apply()
        }
    }

    private fun loadJournalUri(): Uri? {
        val pref = getPreferences(Context.MODE_PRIVATE)
        val uri = pref.getString(JOURNAL_URI, null)
        return if (uri != null) Uri.parse(uri) else null
    }

    @Override
    override fun onBackPressed() {
        if (!navigator.onBackPressed()) {
            super.onBackPressed()
        }
    }
}