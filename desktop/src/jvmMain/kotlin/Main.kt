import androidx.compose.material.MaterialTheme
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import one.apheli.common.LedgyViewModel
import one.apheli.common.MainScreen
import one.apheli.common.model.LedgyColors
import one.apheli.common.navigation.Navigator
import java.awt.FileDialog
import java.awt.Frame
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.prefs.Preferences

fun main() {
    val viewModel = LedgyViewModel()
    val navigator = Navigator()
    navigator.show(
        MainScreen(
            viewModel,
            prepareFileChooser(viewModel),
            ::prepareOutputStream
        )
    )

    val lastJournal = loadJournalPath()
    if (lastJournal.isNotEmpty()) {
        val journalFile = File(lastJournal)
        if (journalFile.exists()) {
            viewModel.importFile(journalFile.inputStream(), lastJournal)
        }
    }

    application {
        Window(onCloseRequest = ::exitApplication, title = "Ledgy") {
            MaterialTheme(colors = LedgyColors.LightColors) {
                navigator.host()
            }
        }
    }
}
fun prepareFileChooser(viewModel: LedgyViewModel): () -> Unit {
    return {
        val dialog = FileDialog(null as Frame?, "Select Journal File")
        dialog.mode = FileDialog.LOAD
        dialog.isVisible = true
        if (dialog.directory != null && dialog.file != null) {
            val path = dialog.directory + dialog.file
            val stream = File(path).inputStream()
            viewModel.importFile(stream, path)
            saveJournalPath(path)
        }
    }
}

fun prepareOutputStream(viewModel: LedgyViewModel): OutputStream? {
    if (File(viewModel.journalPath).exists()) {
        return FileOutputStream(File(viewModel.journalPath), true)
    } else {
        return null
    }
}

fun saveJournalPath(path: String) {
    val prefs = Preferences.userRoot().node("ledgy")
    prefs.put("journalPath", path)
}

fun loadJournalPath(): String {
    val prefs = Preferences.userRoot().node("ledgy")
    return prefs.get("journalPath", "")
}
