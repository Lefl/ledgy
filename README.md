# Ledgy

Ledgy is a double entry accounting app for both Android and Desktop.
It uses hledger-style plain text files and is written using Kotlin and Jetpack Compose.

![Account Tab Screenshot](accountTab.png "Account Tab")
![Transaction Tab Screenshot](transactionTab.png "Transaction Tab")

## 🚧 Not ready for production use 🚧 ##
The App is currently unstable and is missing some features.
It *should* work for *simple* transaction entry
